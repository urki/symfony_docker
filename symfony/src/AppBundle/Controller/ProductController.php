<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Product controller.
 *
 * @Route("p")
 */
class ProductController extends Controller
{
    /**
     * Lists all product entities.
     *
     * @Route("/", name="p_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $products = $em->getRepository('AppBundle:Product')->findAll();

        return $this->render('product/index.html.twig', array(
            'products' => $products,
        ));
    }

    /**
     * Creates a new product entity.
     *
     * @Route("/new", name="p_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $product = new Product();
        $form = $this->createForm('AppBundle\Form\ProductType', $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('p_show', array('id' => $product->getId()));
        }

        return $this->render('product/new.html.twig', array(
            'product' => $product,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a product entity.
     *
     * @Route("/show/{id}", name="p_show")
     * @Method("GET")
     */
    public function showAction(Product $product)
    {
        $deleteForm = $this->createDeleteForm($product);

        return $this->render('product/show.html.twig', array(
            'product' => $product,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing product entity.
     *
     * @Route("/edit/{id}", name="p_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Product $product)
    {
        $deleteForm = $this->createDeleteForm($product);
        $editForm = $this->createForm('AppBundle\Form\ProductType', $product);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('p_edit', array('id' => $product->getId()));
        }

        return $this->render('product/edit.html.twig', array(
            'product' => $product,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a product entity.
     *
     * @Route("/delete/{id}", name="p_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Product $product)
    {
        $form = $this->createDeleteForm($product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($product);
            $em->flush();
        }

        return $this->redirectToRoute('p_index');
    }

    /**
     * Creates a form to delete a product entity.
     *
     * @param Product $product The product entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Product $product)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('p_delete', array('id' => $product->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    private function add_new_unique_product($uniqueNumber = 'BCE9A19269D98E')
    {
        // you can fetch the EntityManager via $this->getDoctrine()
        // or you can add an argument to your action: index(EntityManagerInterface $entityManager)
        $entityManager = $this->getDoctrine()->getManager();

        $product = new Product();
        $product->setProductCode('100170');
        //$product->setUniqueNumber('BCE9A19269D98E');

        $product->setUniqueNumber($uniqueNumber);

        $product->setMaterialLot(array('17-301-000009'));
        $product->setLocationId('2');
        //  $product->setTimestamp("now");
        $product->setSubscriberId('3');


        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($product);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();

      //   return $this->redirectToRoute('product_show', array('id' => $product->getUniqueNumber()));
        //  return new Response('Saved new product with id ' . $product->getId());
        $answer=array('task'=>'Saved new product with id ','productIdAdded'=>$product->getId());
      //  print_r($answer); die;
        return $answer;


    }

    /**
     * @Route("/{id}", name="product_show")
     */
    public function showAction1($id)
    {

        $repository = $this->getDoctrine()->getRepository(Product::class);
        $product = $repository->findOneBy(['uniqueNumber' => $id]);

        if (!$product) {
            $this->add_new_unique_product($id);

            $repository = $this->getDoctrine()->getRepository(Product::class);
            $product = $repository->findOneBy(['uniqueNumber' => $id]);
            // ta dela vendar rabim trenutno editiranje zato sem zakomentiral
            //return $this->redirectToRoute('product_show', array('id' => $id));
            // dodal editiranje po vpsiu
            return $this->redirectToRoute('p_edit', array('id' =>$product->getId()    ));
            //  return $this->redirectToRoute('product', array('unique_number' => $id));
            //  throw $this->createNotFoundException(
            //      'No product found for id '.$id
            // );
        }

       // return new Response('I Check out this great product: ' . $product->getUniqueNumber());//. '<br> <a href="http://docs.vdcsasa.si/app_dev.php/p/'.$product->getId().'/edit">Editiraj</a>');

         return $this->redirectToRoute('p_show', array('id' =>$product->getId()));


        // or render a template
        // in the template, print things with {{ product.name }}
        // return $this->render('product/show.html.twig', ['product' => $product]);
    }



}
