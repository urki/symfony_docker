<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="product_code", type="string", length=20)
     */
    private $productCode;

    /**
     * @var string
     *
     * @ORM\Column(name="unique_number", type="string", length=18)
     */
    private $uniqueNumber;

    /**
     * @var array
     *
     * @ORM\Column(name="material_lot", type="array", nullable=true)
     */
    private $materialLot;

    /**
     * @var int
     *
     * @ORM\Column(name="location_id", type="integer")
     */
    private $locationId;

    /**
     * @var int
     *
     * @ORM\Column(name="subscriber_id", type="integer")
     */
    private $subscriberId;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="string", length=255, nullable=true)
     */
    private $note;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productCode
     *
     * @param string $productCode
     *
     * @return Product
     */
    public function setProductCode($productCode)
    {
        $this->productCode = $productCode;

        return $this;
    }

    /**
     * Get productCode
     *
     * @return string
     */
    public function getProductCode()
    {
        return $this->productCode;
    }

    /**
     * Set uniqueNumber
     *
     * @param string $uniqueNumber
     *
     * @return Product
     */
    public function setUniqueNumber($uniqueNumber)
    {
        $this->uniqueNumber = $uniqueNumber;

        return $this;
    }

    /**
     * Get uniqueNumber
     *
     * @return string
     */
    public function getUniqueNumber()
    {
        return $this->uniqueNumber;
    }

    /**
     * Set materialLot
     *
     * @param array $materialLot
     *
     * @return Product
     */
    public function setMaterialLot($materialLot)
    {
        $this->materialLot = $materialLot;

        return $this;
    }

    /**
     * Get materialLot
     *
     * @return array
     */
    public function getMaterialLot()
    {
        return $this->materialLot;
    }

    /**
     * Set locationId
     *
     * @param integer $locationId
     *
     * @return Product
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;

        return $this;
    }

    /**
     * Get locationId
     *
     * @return int
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * Set subscriberId
     *
     * @param integer $subscriberId
     *
     * @return Product
     */
    public function setSubscriberId($subscriberId)
    {
        $this->subscriberId = $subscriberId;

        return $this;
    }

    /**
     * Get subscriberId
     *
     * @return int
     */
    public function getSubscriberId()
    {
        return $this->subscriberId;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return Product
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }
}

