<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('productCode', ChoiceType::class, array(
            'choices'  => array(
                '500004 Črkovna stavnica' => 500004,
                'Yes' => 1,
                'No' => 2,
            ),))









                ->add('uniqueNumber')



            ->add('materialLot', CollectionType::class, array(
                    'entry_type' => TextType::class,
                    'allow_add' => true,
                    'prototype' => true,
                    'prototype_data' => 'New Tag Placeholder')
            )




                 ->add('locationId', ChoiceType::class, array(
                     'choices'  => array(
                         'Lesarska delavnica' => 1,
                         'Tehnolog' => 1,
                         'Vitrina' => 2,
                     ),))


            ->add('subscriberId', ChoiceType::class, array(
                'choices'  => array(
                    'Uroš Gabrovec' => 3,
                    'Roman Dermol' => 333,
                    'Slavka Petrovic' => 87,
                ),))

            ->add('note');



    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Product'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_product';
    }


}
