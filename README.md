docker spletni stre≈ænik
================================

PHP aplikacija (privzeto je uporabljen framework symfony) je v mapi  [symfony](symfony).

#### PRVA HITRA NAMESTITEV ####
```
sudo nano /etc/hosts
v vrstici `127.0.0.1 localhost` doda≈° ime npr:`127.0.0.1  localhost  sifrant.nalepke.vdcsasa.si`  
cd Desktop
git clone git@bitbucket.org:urki/symfony_docker.git
cd symfony_docker
docker-compose up -d
cd symfony
docker run --rm --interactive --tty --volume $PWD:/app composer install
```
ali (brez dns)
```
cd Desktop && git clone git@bitbucket.org:urki/symfony_docker.git && cd symfony_docker && docker-compose up -d && cd symfony && docker run --rm --interactive --tty --volume $PWD:/app composer install
```
#### Zagon ≈æe name≈°ƒçene
ƒåe bi ≈æe name≈°ƒçen docker ≈æelel ponovno zagnati vpi≈°e≈°
```docker restart my-apache-php-app  myadmin  mysqlserver```

#### Uporaba:
* v brskalniku se na [url](http://sifrant.nalepke.vdcsasa.si) `http://sifrant.nalepke.vdcsasa.si` se odpre stran nalepk
* to iste se lahko pride na istem raƒçunalniku tudi na url `http://localhost`
* phpmyadmin je na [url](http://sifrant.nalepke.vdcsasa.si:8080) `http://sifrant.nalepke.vdcsasa.si:8080` z uporabnikom `root` in geslom `root`

#### Razvoj
* Za pregled/testiranje se uporabi develop razliƒçica z povezavo na: `http://sifrant.nalepke.vdcsasa.si/app_dev.php`
spremembe za izgled so v [symfony/app/Resources/views/Nalepke]()
* ƒåe je blokiran dostop do `app_dev.php` potem  lahko v [symfony/web/app_dev.php] odkomentiras 11 in 12 vrstico:

``` php
//print_r ($_SERVER['REMOTE_ADDR']);
//echo "<hr>";

```
* izpisal se bo ip naslov raƒçunalnika, katerega se doda v [symfony/web/app_dev.php] 20 vrstico.
* Za spremembe je za produkcijo potem izbrisal cache torej `cd symfony`  in `sudo rm -r var/*`

#### Predogled
![alt text](docs/images/nalepke.jpg "Previw")

#### Podrobneje razlo≈æena namestitev in zagon:
 * ureditev DNS
 	* dns hosts na raƒçunalniku - testno-razvojno
	Ne pozabi dodati  IP naslov raƒçunalnika na katerem je name≈°ƒçen docker in  `sifrant.nalepke.vdcsasa.si` v  `/etc/hosts` datoteke raƒçunalnika
(ali pa v DNS stre≈ænik). Ime je lahko tudi drugo, vendar mora biti potem spremenjeno tudi v `base.html.twig`. npr: `sudo nano /etc/config`
in dodamo vanj: ``` 192.168.0.1  sifrant.nalepke.vdcsasa.si```
	* DNS stre≈ænik
* Prenos iz interneta.
gre≈° v mapo kjer bo name≈°ƒçen npr:`cd Desktop` in si klonira≈° symfony_docker v mojem primeru:
`git clone git@bitbucket.org:urki/symfony_docker.git`  (kot drug uporabnik mora≈° imeti napisano drugi ime in pravega protokola
git ali pa https. Tega vidi≈° na vrhu.  Git protokol potrebuje ssh kljuƒç, medtem ko pri https protokolu geslo.

 ![alt text](docs/images/git-clone.png "gitclone")

* namestitev
v mapi kjer je/bo namepƒçen gre≈° v mapo z `cd symfony_docker` in v njej napi≈°e≈°: `docker-compose up -d`
Ko konƒça gre≈° v mapo symfony `cd symfony` in za≈æene≈° ukaz
`docker run --rm --interactive --tty --volume $PWD:/app composer install`

### EROORS?
* ce se ne odpre gre≈° v mapo symfony-docker in na≈pi≈°e≈° `docker-compose down` in `docker compose up -d`
* preveri ce je na istem portu! Je mogoce 81? 


### Linki
#### php apc in int - tezava docker:
* https://hub.docker.com/r/neolao/php/~/dockerfile/
* http://blog.cloud66.com/deploying-your-laravel-php-applications-with-cloud-66/

### @TODO





